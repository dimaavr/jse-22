package ru.tsc.avramenko.tm.service;

import ru.tsc.avramenko.tm.api.repository.IOwnerRepository;
import ru.tsc.avramenko.tm.api.service.IOwnerService;
import ru.tsc.avramenko.tm.exception.empty.EmptyIdException;
import ru.tsc.avramenko.tm.exception.system.ProcessException;
import ru.tsc.avramenko.tm.model.AbstractOwnerEntity;

import java.util.List;

public abstract class AbstractOwnerService<E extends AbstractOwnerEntity> extends AbstractService<E>
        implements IOwnerService<E> {

    private final IOwnerRepository<E> repository;

    public AbstractOwnerService(IOwnerRepository<E> repository) {
        super(repository);
        this.repository = repository;
    }

    @Override
    public E add(final String userId, final E entity) {
        if (entity == null) return null;
        return repository.add(userId, entity);
    }

    @Override
    public void remove(final String userId, final E entity) {
        if (entity == null) throw new ProcessException();
        repository.remove(userId, entity);
    }

    @Override
    public List<E> findAll(final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        return repository.findAll(userId);
    }

    @Override
    public void clear(final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        repository.clear(userId);
    }

    @Override
    public E findById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.findById(userId, id);
    }

    @Override
    public E removeById(final String userId, final String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        return repository.removeById(userId, id);
    }

}