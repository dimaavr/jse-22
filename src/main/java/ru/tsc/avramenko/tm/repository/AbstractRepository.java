package ru.tsc.avramenko.tm.repository;

import ru.tsc.avramenko.tm.api.IRepository;
import ru.tsc.avramenko.tm.exception.system.ProcessException;
import ru.tsc.avramenko.tm.model.AbstractEntity;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    protected final List<E> list = new ArrayList<>();

    @Override
    public void add(final E entity) {
        list.add(entity);
    }

    @Override
    public void remove(final E entity) {
        list.remove(entity);
    }

    @Override
    public List<E> findAll() {
        return list;
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public E findById(final String id) {
        return list.stream()
                .filter(e -> e.getId().equals(id))
                .findFirst()
                .orElseThrow(ProcessException::new);
    }

    @Override
    public E removeById(final String id) {
        final E entity = findById(id);
        if (entity == null) return null;
        list.remove(entity);
        return entity;
    }
}