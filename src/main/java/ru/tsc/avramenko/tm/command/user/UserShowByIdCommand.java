package ru.tsc.avramenko.tm.command.user;

import ru.tsc.avramenko.tm.command.AbstractUserCommand;
import ru.tsc.avramenko.tm.enumerated.Role;
import ru.tsc.avramenko.tm.exception.entity.UserNotFoundException;
import ru.tsc.avramenko.tm.exception.system.AccessDeniedException;
import ru.tsc.avramenko.tm.model.User;
import ru.tsc.avramenko.tm.util.TerminalUtil;

public class UserShowByIdCommand extends AbstractUserCommand {

    @Override
    public String name() {
        return "user-show-by-id";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show user by id.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER USER ID: ");
        final String id = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().findById(id);
        if (user == null) throw new UserNotFoundException();
        showUser(user);
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}