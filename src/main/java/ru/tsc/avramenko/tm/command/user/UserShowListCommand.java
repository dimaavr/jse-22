package ru.tsc.avramenko.tm.command.user;

import ru.tsc.avramenko.tm.command.AbstractUserCommand;
import ru.tsc.avramenko.tm.enumerated.Role;
import ru.tsc.avramenko.tm.exception.system.ProcessException;
import ru.tsc.avramenko.tm.model.User;

import java.util.List;

public class UserShowListCommand extends AbstractUserCommand {

    @Override
    public String name() {
        return "user-list";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Show all users.";
    }

    @Override
    public void execute() {
        System.out.println("List of users.");
        final List<User> users = serviceLocator.getUserService().findAll();
        if (users == null) throw new ProcessException();
        int index = 1;
        for (final User user : users) {
            System.out.println("- - - № " + index + " - - -");
            showUser(user);
            index++;
        }
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}