package ru.tsc.avramenko.tm.command.task;

import ru.tsc.avramenko.tm.command.AbstractTaskCommand;
import ru.tsc.avramenko.tm.enumerated.Role;
import ru.tsc.avramenko.tm.exception.entity.TaskNotFoundException;
import ru.tsc.avramenko.tm.model.Task;
import ru.tsc.avramenko.tm.util.TerminalUtil;

public class TaskRemoveByNameCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-remove-by-name";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove task by name.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        final Task task = serviceLocator.getTaskService().findByName(userId, name);
        if (task == null) throw new TaskNotFoundException();
        serviceLocator.getTaskService().removeByName(userId, name);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}