package ru.tsc.avramenko.tm.command.task;

import ru.tsc.avramenko.tm.command.AbstractTaskCommand;
import ru.tsc.avramenko.tm.enumerated.Role;
import ru.tsc.avramenko.tm.exception.entity.TaskNotFoundException;
import ru.tsc.avramenko.tm.model.Task;
import ru.tsc.avramenko.tm.util.TerminalUtil;

public class TaskRemoveByIndexCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-remove-by-index";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Remove task by index.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        final Task task = serviceLocator.getTaskService().findByIndex(userId, index);
        if (task == null) throw new TaskNotFoundException();
        serviceLocator.getTaskService().removeByIndex(userId, index);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}