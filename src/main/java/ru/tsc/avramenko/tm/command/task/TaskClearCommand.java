package ru.tsc.avramenko.tm.command.task;

import ru.tsc.avramenko.tm.command.AbstractTaskCommand;
import ru.tsc.avramenko.tm.enumerated.Role;

public class TaskClearCommand extends AbstractTaskCommand {

    @Override
    public String name() {
        return "task-clear";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Removing all tasks.";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR TASK]");
        final String userId = serviceLocator.getAuthService().getCurrentUserId();
        serviceLocator.getTaskService().clear(userId);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return Role.values();
    }

}