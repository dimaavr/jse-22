package ru.tsc.avramenko.tm.api.repository;

import ru.tsc.avramenko.tm.api.IRepository;
import ru.tsc.avramenko.tm.model.AbstractOwnerEntity;

import java.util.List;

public interface IOwnerRepository<E extends AbstractOwnerEntity> extends IRepository<E> {

    E add(String userId, final E entity);

    void remove(String userId, final E entity);

    List<E> findAll(String userId);

    void clear(String userId);

    E findById(String userId, final String id);

    E removeById(String userId, final String id);

}
